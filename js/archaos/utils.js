﻿/// <reference path="C:\wamp\www\public\archaos\js\archaos\refs.js">

/*** 
* Debounced event listener
* Created by @laustdeleuran and @emilchristensen
*
* Code hijacked and inspired by 
* http://paulirish.com/2009/throttled-smartresize-jquery-event-handler/ 
* which in turn was inspired by 
* http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
*
* Tested successfully in:
* - Chrome 13, Windows 7 
* - FireFox 5, Windows 7 
* - Opera 11.5, Windows 7
* - Safari 5.1, Windows 7
* - Internet Explorer 9, Windows 7
*
* IE6-8 (only tested in IE8) will run head-first into this problem:
* http://javascriptfixer.com/member-not-found.php
* when passing an eventObject through the function bound via smartbind.
* See also:
* http://stackoverflow.com/questions/3531751/member-not-found-ie-error-ie-6-7-8
***/
(function ($, smartbind) {
    var debounce = function (func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this, args = arguments;
            function delayed() {
                func.apply(obj, args);
            }

            if (timeout) {
                clearTimeout(timeout);
            } else if (execAsap) {
                func.apply(obj, args);
                return;
            }
            timeout = setTimeout(delayed, threshold || 200);
        };
    };
    // Smartbind
    Zepto.fn[smartbind] = function (event, func, threshold, execAsap) { return func ? this.bind(event, debounce(func, threshold, execAsap)) : this.trigger(event); };

})(Zepto, 'smartbind');

/**
 * IE Console shim
 */
if (typeof window.console === "undefined") {
    window.console = {};
    window.console.log = function () {
        return;
    };
    window.console.warn = function () {
        return;
    };
    window.console.error = function () {
        return;
    };
}

Archaos.Utils = (function () {
    /**
     * Some handy utility functions.
     * 
     * @class Archaos.Utils
     */
    function Utils() {

    }

    /**
     * Create a custom base64-esque version of a number.
     * 
     * @method Archaos.Utils.shortify
     * @param {number} num The number to shortify.
     * @return {string} The shortified number.
     * @static
     */
    Utils.shortify = function (num) {
        var list = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-", dec = 0, i;

        num = num.toString();
        for (i = 0; i <= num.length; i++) {
            dec += (list.indexOf(num.charAt(i))) * (Math.pow(10, (num.length - i - 1)));
        }
        num = "";
        var magnitude = Math.floor((Math.log(dec)) / (Math.log(64)));
        for (i = magnitude; i >= 0; i--) {
            var amount = Math.floor(dec / Math.pow(64, i));
            num = num + list.charAt(amount);
            dec -= amount * (Math.pow(64, i));
        }
        return num || "0";
    };

    /**
     * Return the padded hex version of a number.
     * 
     * @method Archaos.Utils.decimalToHex
     * @param {number} num The number to convert.
     * @param {number} [padding=2] The minimum size to pad the value to.
     * @return {string} The converted number.
     * @static
     */
    Utils.decimalToHex = function (num, padding) {
        var hex = Number(num).toString(16);
        padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;

        while (hex.length < padding) {
            hex = "0" + hex;
        }

        return hex;
    };

    return Utils;
})();

/**
* Calculates the centroid (or midpoint) from an array of points. If only one point is provided, that point is returned.
* @method Phaser.Point.centroid
* @param {Phaser.Point[]} points - The array of one or more points.
* @param {Phaser.Point} [out] - Optional Point to store the value in, if not supplied a new Point object will be created.
* @return {Phaser.Point} The new Point object.
*/
Phaser.Point.centroid = function (points, out) {

    if (typeof out === "undefined") { out = new Phaser.Point(); }

    if (Object.prototype.toString.call(points) !== '[object Array]') {
        throw new Error("Phaser.Point. Parameter 'points' must be an array");
    }

    var pointslength = points.length;

    if (pointslength < 1) {
        throw new Error("Phaser.Point. Parameter 'points' array must not be empty");
    }

    if (pointslength === 1) {
        out.copyFrom(points[0]);
        return out;
    }

    for (var i = 0; i < pointslength; i++) {
        Phaser.Point.add(out, points[i], out);
    }

    out.divide(pointslength, pointslength);

    return out;
};

// FIX: Replace _generateCachedSprite function to use correct bounds.
PIXI.DisplayObject.prototype._generateCachedSprite = function ()//renderSession)
{
    this._cacheAsBitmap = false;
    var bounds = this.getLocalBounds();

    if (!this._cachedSprite) {
        var renderTexture = new PIXI.RenderTexture(bounds.width | 0, bounds.height | 0);//, renderSession.renderer);

        this._cachedSprite = new PIXI.Sprite(renderTexture);
        this._cachedSprite.worldTransform = this.worldTransform;
    } else {
        this._cachedSprite.texture.resize(bounds.width | 0, bounds.height | 0);
    }

    //REMOVE filter!
    var tempFilters = this._filters;
    this._filters = null;

    this._cachedSprite.filters = tempFilters;
    this._cachedSprite.texture.render(this, new PIXI.Point(-bounds.x, -bounds.y));

    this._filters = tempFilters;

    this._cacheAsBitmap = true;
};
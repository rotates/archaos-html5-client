﻿/// <reference path="C:\wamp\www\public\archaos\js\archaos\refs.js">

Archaos.Player = (function () {
    /**
     * Creates a new instance of an <code>Archaos.Player</code> object.
     * 
     * @class Archaos.Player
     * @param {number} index The index of this player.
     * @param {object} data The data for this player.
     */
    function Player(index, data) {
        this.index = index;
        this.username = data.username;
        this.handle = data.handle;
        this.defeated = data.defeated;
        this.current = false;
    }

    /**
     * Serializes the player.
     * 
     * @method Archaos.Player#serialize
     * @return {object} The serialized player.
     */
    Player.prototype.serialize = function () {
        var output = {
            username: this.username,
            handle: this.handle,
            defeated: this.defeated
        };
        return output;
    };

    return Player;
})();
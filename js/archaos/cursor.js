/// <reference path="C:\wamp\www\public\archaos\js\archaos\refs.js">

Archaos.Cursor = (function () {
    /**
     * Creates a new instance of an <code>Archaos.Cursor</code> object.
     * 
     * @class Archaos.Cursor
     * @param {Archaos.Board} board The board this cursor belongs to.
     */
    function Cursor(board) {
        this.board = board;
        this.sprite = Archaos.game.add.sprite(0, 0, 'cursors', Cursor.IDLE);

        this.selectedPos = new Phaser.Point( -1, -1);
        this.prevCursorPos = new Phaser.Point( -1, -1);
        this.pos = new Phaser.Point(0, 0);
        this.cursorPosScreen = new Phaser.Point(0, 0);
        this.cursorPosIso = new Phaser.Point(0, 0);

        this.getCursorPos();

        this.x = this.pos.x;
        this.y = this.pos.y;
    }

    // Consts
    Cursor.IDLE = "idle";
    Cursor.MOVE = "move";
    Cursor.UP = "up";
    Cursor.UP_RIGHT = "up-right";
    Cursor.RIGHT = "right";
    Cursor.DOWN_RIGHT = "down-right";
    Cursor.DOWN = "down";
    Cursor.DOWN_LEFT = "down-left";
    Cursor.LEFT = "left";
    Cursor.UP_LEFT = "up-left";
    Cursor.FLY = "fly";
    Cursor.ATTACK = "attack";
    Cursor.RANGED_ATTACK = "rangedattack";
    Cursor.CAST = "cast";
    Cursor.INVALID = "invalid";
    Cursor.SELECT = "select";
    Cursor.INFO = "info";
    Cursor.MOUNT = "mount";
    Cursor.DISMOUNT = "dismount";
    Cursor.WARNING = "warning";

    /**
     * Gets the current position of the cursor in board tile coordinates.
     * 
     * @method Archaos.Cursor#getCursorPos
     * @param {number} x The x position in screen space.
     * @param {number} y The y position in screen space.
     * @return {Phaser.Point} The resolved tile coordinates.
     */
    Cursor.prototype.getCursorPos = function (x, y) {
        if (x === this.cursorPosScreen.x && y === this.cursorPosScreen.y) {
            return;
        }
        this.cursorPosScreen.setTo(x, y);
        this.pos.setTo(x, y);
        this.pos = this._translateCursorPos(this.pos);

        if (Phaser.Point.equals(this.pos, this.prevCursorPos)) {
            return;
        }

        this.prevCursorPos.copyFrom(this.pos);

        if (!this.board.isValidTile(this.pos.x, this.pos.y)) {
            this.sprite.alpha = 0.5;
        } else {
            this.sprite.alpha = 1;
        }
        this.cursorPosIso.copyFrom(this.pos);
        this.cursorPosIso = Archaos.Board.toIso(this.cursorPosIso);
        this.sprite.position.setTo(this.cursorPosIso.x - 1, this.cursorPosIso.y - Archaos.Board.TILESIZE);
        
        var occupied = this.board.getUnitAt(this.pos.x, this.pos.y);
        if (!occupied) {
            this.setCursor(Cursor.IDLE);
            this.board.updateInfo(null);
        } else {
            this.board.updateInfo(occupied);
            if (occupied && !occupied.moved && occupied.owner === this.board.currentPlayer) {
                this.setCursor(Cursor.SELECT);
            } else {
                this.setCursor(Cursor.INVALID);
            }
        }

        if (this.sprite.alpha === 1) {
            this.board.sort();
        }
        this.x = this.pos.x;
        this.y = this.pos.y;

        return this.pos;
    };

    /**
     * Sets the cursor to the specified type.
     * 
     * @method Archaos.Cursor#setCursor
     * @param {string} type The desired cursor type.
     */
    Cursor.prototype.setCursor = function (type) {
        this.sprite.frameName = type;
        if ([Cursor.IDLE, Cursor.UP, Cursor.UP_RIGHT, Cursor.RIGHT, Cursor.DOWN_RIGHT, Cursor.DOWN, Cursor.DOWN_LEFT, Cursor.LEFT, Cursor.UP_LEFT].indexOf(type) === -1) {
            this.sprite.order = Archaos.Board.TILESIZE * 0.6;
        } else {
            this.sprite.order = 0; 
        }
    };

    /**
     * Translates screen space coordinates into board tile coordinates.
     * 
     * @method Archaos.Cursor#_translateCursorPos
     * @param {Phaser.Point} point The screen space coordinates to translate.
     * @return {Phaser.Point} The resolved tile coordinates.
     * @private
     */
    Cursor.prototype._translateCursorPos = function (point) {
        point.x -= (this.board.group.x * Archaos.SCALE) - (Archaos.Board.TILESIZE);
        point.y -= (this.board.group.y * Archaos.SCALE) - (Archaos.Board.TILESIZE * (2 / Archaos.SCALE));

        var ly = (((2 * point.y - point.x) / 2) - (Archaos.Board.TILESIZE));
        var lx = (point.x + ly) - (Archaos.Board.TILESIZE);
        var ay = Math.round(ly / (Archaos.Board.TILESIZE)) - 1;
        var ax = Math.round(lx / (Archaos.Board.TILESIZE)) - 1;
        point.x = Math.round(ax / Archaos.SCALE);
        point.y = Math.round(ay / Archaos.SCALE);
        return point;
    };

    return Cursor;
})();

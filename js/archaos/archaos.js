/// <reference path="C:\wamp\www\public\archaos\js\archaos\refs.js">
var Archaos = (function () {
    /**
     * The main Archaos class which preloads assets and launches games.
     * 
     * @class Archaos
     */
    function Archaos() {

        /** @property {Phaser.Game} game A reference to the currently running game. */
        Archaos.game = new Phaser.Game($(window).width(), $(window).height(), Phaser.AUTO, "game", null, true, false);
        Archaos.game.state.add('Preload', this.preloadState);
        Archaos.game.state.add('Intro', this.introState);
        Archaos.game.state.add('Game', this.gameState);
        Archaos.game.state.start('Preload');
    }

    // Consts
    Archaos.SCALE = 2;
    Archaos.GLOBALVOLUME = 1;

    /**
     * Preload state
     * 
     * @method Archaos#preloadState
     * @param {Phaser.Game} game The active <code>Phaser.Game</code> instance.
     */
    Archaos.prototype.preloadState = function (game) {
        this.preload = function () {
            game.world.scale.setTo(Archaos.SCALE, Archaos.SCALE);
            // Preload assets
            game.load.bitmapFont('font-archaos', 'assets/archaos-font.png', 'assets/archaos-font.xml');
            game.load.image('logo', 'assets/logo.png');
            game.load.atlasJSONArray('board', 'assets/board.png', 'assets/board.json');
            game.load.atlasJSONArray('cursors', 'assets/cursors.png', 'assets/cursors.json');
            game.load.atlasJSONArray('units-classic', 'assets/units-classic.png', 'assets/units-classic.json');
            game.load.spritesheet('wizards', 'assets/wizards.png', 36, 18);
            game.load.spritesheet('hats', 'assets/hats.png', 28, 14);
            game.load.audio('attack', ['assets/sounds/attack.ogg', 'assets/sounds/attack.mp3']);
            game.load.audio('cancel', ['assets/sounds/cancel.ogg', 'assets/sounds/cancel.mp3']);
            game.load.audio('endturn', ['assets/sounds/endturn.ogg', 'assets/sounds/endturn.mp3']);
            game.load.audio('engaged', ['assets/sounds/engaged.ogg', 'assets/sounds/engaged.mp3']);
            game.load.audio('fly', ['assets/sounds/fly.ogg', 'assets/sounds/fly.mp3']);
            game.load.audio('kill', ['assets/sounds/kill.ogg', 'assets/sounds/kill.mp3']);
            game.load.audio('move', ['assets/sounds/move.ogg', 'assets/sounds/move.mp3']);
            game.load.audio('rangedattack', ['assets/sounds/rangedattack.ogg', 'assets/sounds/rangedattack.mp3']);
            game.load.audio('select', ['assets/sounds/select.ogg', 'assets/sounds/select.mp3']);

            $(window).smartbind('resize', function () { Archaos.updateSize(); }, 100);
            Archaos.updateSize();
        };

        this.create = function () {

            game.sounds = {
                "attack": game.add.audio('attack'),
                "cancel": game.add.audio('cancel'),
                "endturn": game.add.audio('endturn'),
                "engaged": game.add.audio('engaged'),
                "fly": game.add.audio('fly'),
                "kill": game.add.audio('kill'),
                "move": game.add.audio('move'),
                "rangedattack": game.add.audio('rangedattack'),
                "select": game.add.audio('select')
            };

            game.sound.channels = 1;

            for (var s in game.sounds) {
                if (game.sounds[s].hasOwnProperty("override")) {
                    game.sounds[s].override = true;
                }
            }

            // Basic Phaser setup
            game.input.maxPointers = 1;
            game.stage.disableVisibilityChange = true;
            game.stage.smoothed = false;
            if (game.renderType === Phaser.CANVAS) {
                Phaser.Canvas.setSmoothingEnabled(game.context, false);
            }
            game.state.start('Game');
        };
    };

    /**
     * Intro state
     * 
     * @method Archaos#introState
     * @param {Phaser.Game} game The active <code>Phaser.Game</code> instance.
     */
    Archaos.prototype.introState = function (game) {
        this.preload = function () {
            // Preload
            game.add.audio('fly').play();
            this.logoWrapper = game.add.group();
            this.logoWrapper.x = game.width * 0.5;
            this.logoWrapper.y = game.height * 0.5;
            this.logo = game.add.image(0, 0, 'logo', 0, this.logoWrapper);
            this.logo.anchor.setTo(0.5, 0.5);
            this.logo.y = -70;
            this.logo.alpha = 0;

            game.add.tween(this.logo).to({ alpha: 1 }, 600, Phaser.Easing.Quadratic.Out, true);
            game.add.tween(this.logo).to({ y: 70 }, 1000, Phaser.Easing.Bounce.Out, true);
        };

        this.create = function () {
            var self = this;
            var text = game.add.bitmapText(0, 128, 'font-archaos', (game.device.desktop ? 'Click' : 'Tap') + ' to start', 13, this.logoWrapper);
            text.x -= text.getLocalBounds().width * 0.5;
            text.alpha = 0;
            game.add.tween(text).to({ alpha: 1 }, 300, null, true, 1500);

            game.input.onDown.add(function () {
                game.add.tween(text).to({ alpha: 0 }, 300, null, true);
                var t = game.add.tween(this.logo).to({ alpha: 0, y: "+40" }, 600, Phaser.Easing.Quartic.In);
                t.onComplete.add(function () {
                    self.logoWrapper.destroy(true);
                    text.destroy(true);
                    game.state.start('Game');
                });
                t.start();
                game.add.audio('kill').play();
            }, this);
        };

        this.update = function () {
            this.logoWrapper.x = game.width * 0.5;
            this.logoWrapper.y = game.height * 0.2;
        };
    };

    /**
     * Game state
     * 
     * @method Archaos#gameState
     * @param {Phaser.Game} game The active <code>Phaser.Game</code> instance.
     */
    Archaos.prototype.gameState = function (game) {
        this.preload = function () {
            // Preload
            game.load.json('unitSpriteData', 'data/classicunits.json');
            game.load.json('unitData', 'http://82.4.14.208:3000/data/units');
            
        };

        this.create = function () {
            var self = this;
            Archaos.Unit.unitSpriteData = game.cache.getJSON('unitSpriteData');
            Archaos.Unit.unitData = game.cache.getJSON('unitData');
            this.board = null;

            $.ajax({
                url: 'http://82.4.14.208:3000/games/3/',
                data: { username: 'lewster32', token: '4205d8e315d3962232d1e28d1991997f88e5c518' },
                dataType: 'json',
                success: function (data) {
                    console.log("Incoming data:", data.response.success);
                    self.board = Archaos.Board.create(data.response.success, false);
                    console.log("Serialized board:", self.board.serialize());
                }
            });  
        };

        this.update = function () {
            if (this.board) {
                this.board.update();
            }
        };

        this.render = function () {
            // game.debug.inputInfo(16, 48);
            if (this.board) {
                game.debug.text(this.board.cursor.x + ", " + this.board.cursor.y, 16, 24, "#fff", "font-archaos");
            }
        };
    };

    /**
     * Update the size of the game and stage when the window is resized.
     * 
     * @method Archaos.updateSize
     * @static
     */
    Archaos.updateSize = function () {
        var width = $(window).width(),
            height = $(window).height(),
            halfWidth = Math.round(width / Archaos.game.world.scale.x),
            halfHeight = Math.round(height / Archaos.game.world.scale.y);

        Archaos.game.width = Archaos.game.stage.bounds.width = halfWidth;
        Archaos.game.height = Archaos.game.stage.bounds.height = halfHeight;

        Archaos.game.renderer.resize(width, height);

        if (Archaos.game.renderType === Phaser.CANVAS) {
            Phaser.Canvas.setSmoothingEnabled(Archaos.game.context, false);
        }
        
    };

    return Archaos;
})();

new Archaos();